import React from "react";
import "../style/scss/WeatherForecast.scss";
import {
  convertKelvinToCelsius,
  convertKelvinToFahrenheit,
} from "../utils/temperature-converter";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

export default class WeatherForecast extends React.Component {
  getForecast = (dayData) => {
    if (dayData.length > 0) {
      return dayData.map((each, index) => {
        return (
          <Col className="forecast-day-container " id={index} key={index}>
            <Row className="">
              <i className={`wi ${each.weather}`}></i>
            </Row>
            <Row>
              <h3>{each.dayABV}</h3>
            </Row>
            <Row className="justify-content-center">
              {this.props.useCelsius
                ? Math.round(convertKelvinToCelsius(each.maxTemp))
                : Math.round(convertKelvinToFahrenheit(each.maxTemp))}
              &deg;
            </Row>
            <Row className="low-temp justify-content-center">
              {this.props.useCelsius
                ? Math.round(convertKelvinToCelsius(each.minTemp))
                : Math.round(convertKelvinToFahrenheit(each.minTemp))}
              &deg;
            </Row>
          </Col>
        );
      });
    }
  };

  render() {
    if (
      Object.entries(this.props.forecastData).length === 0 &&
      this.props.forecastData.constructor === Object
    ) {
      return (
        <Row>
          <p>Loading...</p>
        </Row>
      );
    } else {
      return (
        <Row className="forecast-container text-center p-0 mt-4">
          {this.getForecast(this.props.forecastData.forecastDays)}
        </Row>
      );
    }
  }
}
