import React from "react";
import "../style/scss/WeatherDaily.scss";
import "../style/css/weather-icons.css";
import {
  convertKelvinToCelsius,
  convertKelvinToFahrenheit,
} from "../utils/temperature-converter";
import { meterToMiles } from "../utils/constants/variables";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

export default class WeatherDaily extends React.Component {
  render() {
    const result = this.props.weatherData;
    if (typeof result.weather !== "undefined")
      result.weather =
        result.weather.charAt(0).toUpperCase() +
        this.props.weatherData.weather.slice(1);
    return (
      <Container className="weather-daily-container">
        <Row>
          <i className={`wi ${this.props.weatherData.icon}`}></i>
        </Row>
        <Row className="weather-daily-info-container align-items-center">
          <Col className="">
            <p>
              {this.props.useCelsius
                ? Math.round(
                    convertKelvinToCelsius(this.props.weatherData.temperature)
                  )
                : Math.round(
                    convertKelvinToFahrenheit(
                      this.props.weatherData.temperature
                    )
                  )}
              {this.props.useCelsius ? "\xB0C" : "\xB0F"}
            </p>
            <button
              name="toggle-temperature-button"
              id="toggle-temperature-button"
              onClick={this.props.onTemperatureChange}
            >
              {this.props.useCelsius ? "| F" : "| C"}
            </button>
          </Col>
          <Col className="weather-atmosphere-info-container text-end">
            <Row className="">
              <Col className="">
                <i className="wi wi-strong-wind"></i>
                <p>
                  {this.props.useCelsius
                    ? this.props.weatherData.windSpeed
                    : Math.round(
                        this.props.weatherData.windSpeed * meterToMiles
                      )}
                  {this.props.useCelsius ? "m/s" : " mph"}
                </p>
              </Col>
            </Row>
            <Row>
              <Col>
                <i className="wi wi-humidity"></i>
                <p>{this.props.weatherData.humidity}%</p>
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    );
  }
}
