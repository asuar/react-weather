/* global google */
/*defines google for eslint or wont be recognized
  alternatives:
  const google = window.google;
  new window.google.maps.LatLngBounds()
*/
import React from "react";
import "../style/scss/WeatherContainer.scss";
import "../style/scss/Input.scss";
import WeatherDaily from "./WeatherDaily.jsx";
import WeatherForecast from "./WeatherForecast.jsx";
import {
  getCurrentWeather,
  getWeatherForecast,
  getLocationImage,
} from "../api/fetch-weather";
import googleCopyright from "../images/powered_by_google_on_non_white_hdpi.png";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Card from "react-bootstrap/Card";

export default class WeatherContainer extends React.Component {
  state = {
    weatherData: {},
    locationImage: {},
    forecastData: {},
    placeholderText: "Zip Code or City, State",
    useCelsius: false,
    googleMapsPromise: {},
  };

  componentDidMount() {
    this.getGoogleMaps().then((google) => {
      this.updateLocationImage(undefined, google);
    });
    this.updateWeather();
    this.updateForecast();
  }

  getGoogleMaps = () => {
    if (!this.googleMapsPromise) {
      this.googleMapsPromise = new Promise((resolve) => {
        // Add a global handler for when the API finishes loading
        window.resolveGoogleMapsPromise = () => {
          // Resolve the promise
          resolve(google);

          // Tidy up
          delete window.resolveGoogleMapsPromise;
        };

        // Load the Google Maps API
        const script = document.createElement("script");
        script.src = `https://maps.googleapis.com/maps/api/js?key=${process.env.REACT_APP_GOOGLE_API_ID}&libraries=places&callback=resolveGoogleMapsPromise`;
        script.async = true;
        script.defer = true;
        document.body.appendChild(script);
      });
    }

    // Return a promise for the Google Maps API
    return this.googleMapsPromise;
  };

  async updateWeather(cityNameInput) {
    const weatherData = await getCurrentWeather(cityNameInput);
    if (weatherData !== "city not found") {
      this.setState({
        weatherData: weatherData,
      });
    } else {
      alert("City not found");
    }
  }

  async updateForecast(cityNameInput) {
    const data = await getWeatherForecast(cityNameInput);
    if (data !== "city not found") {
      this.setState({
        forecastData: data,
      });
    }
  }

  async updateLocationImage(cityNameInput, googleMap) {
    const image = await getLocationImage(cityNameInput, googleMap);
    if (image !== undefined) {
      this.setState({
        locationImage: image,
      });
    }
  }

  async handleChange(e) {
    this.updateWeather(e);
    this.updateForecast(e);
    this.getGoogleMaps().then((google) => {
      this.updateLocationImage(e, google);
    });
  }

  keyPress = (e) => {
    if (e.keyCode === 13) {
      this.handleChange(e.target.value);
      e.target.value = "";
    }
  };

  onTemperatureChange = () => {
    this.setState({
      useCelsius: !this.state.useCelsius,
    });
  };

  render() {
    return (
      <Container>
        <Row className="justify-content-center weather-container-wrapper">
          <Card className="weather-container p-0 border-0">
            <Card.Img
              className="weather-image"
              src={this.state.locationImage}
              alt="Weather image"
            />
            <Card.ImgOverlay>
              <Card.Header className="border-0 location-info-container">
                <p>
                  {this.state.weatherData.city},{" "}
                  {this.state.weatherData.country}
                </p>
                <p>{this.state.weatherData.time}</p>
                <p>{this.state.weatherData.weather}</p>
              </Card.Header>
              <Card.Body className="weather-info-container">
                <WeatherDaily
                  weatherData={this.state.weatherData}
                  useCelsius={this.state.useCelsius}
                  onTemperatureChange={this.onTemperatureChange}
                />
                <WeatherForecast
                  forecastData={this.state.forecastData}
                  useCelsius={this.state.useCelsius}
                />
              </Card.Body>
              <Card.Footer className="input-container border-0 text-center pt-0">
                <div>
                  <input
                    type="text"
                    name="LocationInput"
                    id="LocationInput"
                    className="hvr-grow"
                    placeholder={this.state.placeholderText}
                    onKeyDown={this.keyPress}
                  />
                </div>
                <img
                  className="footer-image"
                  src={googleCopyright}
                  alt="Powered by Google"
                />
              </Card.Footer>
            </Card.ImgOverlay>
          </Card>
        </Row>
      </Container>
    );
  }
}
