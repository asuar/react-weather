export const getWeatherIcon = (weatherID, currentTime) => {
  if (weatherID >= 200 && weatherID <= 232) return "wi-thunderstorm"; //thunderstorms
  if (weatherID >= 300 && weatherID <= 321) return "wi-showers"; //drizzles
  if (weatherID === 511) return "wi-snow"; //freezing rain
  if (weatherID >= 500 && weatherID <= 504) return "wi-rain"; //rain
  if (weatherID >= 520 && weatherID <= 531) return "wi-showers"; //showers
  if (weatherID >= 600 && weatherID <= 602) return "wi-snowflake-cold"; //snow
  if (weatherID >= 611 && weatherID <= 613) return "wi-sleet"; //sleet
  if (weatherID >= 615 && weatherID <= 622) return "wi-snow"; //snow and shower or rain

  if (weatherID === 701) return "wi-fog"; //mist
  if (weatherID === 711) return "wi-smoke"; //smoke
  if (weatherID === 721) return "wi-day-haze"; //Haze
  if (weatherID === 731) return "wi-sandstorm"; //sand or dust whirls
  if (weatherID === 741) return "wi-fog"; //Fog
  if (weatherID >= 751 && weatherID <= 761) return "wi-dust"; //Sand, Dust
  if (weatherID === 762) return "wi-volcano"; //Ash
  if (weatherID === 771) return "wi-strong-wind"; //Squall
  if (weatherID === 782) return "wi-tornado"; //Tornado
  if (weatherID === 800) {
    if (
      (currentTime.slice(-2) === "PM" && currentTime.slice(-8, -6) >= "6") ||
      (currentTime.slice(-2) === "AM" && currentTime.slice(-8, -6) <= "6") ||
      currentTime.slice(-8, -6) === "12" ||
      currentTime.slice(-8, -6) >= "18" ||
      currentTime.slice(-8, -6) <= "6"
    )
      return "wi-night-clear";
    else return "wi-day-sunny"; //Clear
  }

  if (weatherID >= 801 && weatherID <= 802) return "wi-cloud"; //Few Clouds, Scattered Clouds
  if (weatherID >= 803 && weatherID <= 804) return "wi-cloudy"; //Broken Clouds, Overcast Clouds

  return "wi-na"; //failure
};
