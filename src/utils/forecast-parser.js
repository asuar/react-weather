import { getWeatherIcon } from "./weather-icons";

export const getTemperatureExtremes = (list, startIndex, endIndex) => {
  let count = startIndex;
  let min = list[count].main.temp_min;
  let max = list[count].main.temp_max;

  while (count < endIndex && count < list.length) {
    if (list[count].main.temp_min < min) {
      min = list[count].main.temp_min;
    }
    if (list[count].main.temp_max > max) {
      max = list[count].main.temp_max;
    }
    count++;
  }
  return { min, max };
};

export const getDailyWeather = (list, startIndex, time) => {
  if (startIndex + 4 < list.length) {
    return getWeatherIcon(list[startIndex + 4].weather[0].id, time);
  }
};
