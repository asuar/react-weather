import { daysOfTheWeek } from "./constants/variables";

export const getDateFromTimeZone = (timezone) => {
  var today = new Date(new Date().getTime());
  today.setTime(today.getTime() + timezone * 1000);

  var date = daysOfTheWeek[today.getUTCDay()];
  var time = getNormalTimeFormat(today.toUTCString());
  return date + " " + time;
};

export const getDayFromDate = (time, useAbbreviation) => {
  var today = new Date(time);
  if (useAbbreviation) return daysOfTheWeek[today.getUTCDay()].slice(0, 3);
  else return daysOfTheWeek[today.getUTCDay()];
};

const getNormalTimeFormat = (dateString) => {
  var date = new Date(dateString);
  var hours = date.getUTCHours();
  var minutes = date.getUTCMinutes();
  //var seconds = date.getUTCSeconds();
  var timeAbbreviation = "AM";
  if (hours >= 12) {
    hours = hours - 12;
    timeAbbreviation = "PM";
  }
  if (hours === 0) {
    hours = 12;
  }
  minutes = minutes < 10 ? "0" + minutes : minutes;
  //seconds = seconds < 10 ? "0" + seconds : seconds;
  return hours + ":" + minutes + " " + timeAbbreviation;
};
