export const convertKelvinToCelsius = temperature => {
  return Math.round((temperature - 273.15) * 10) / 10;
};

export const convertKelvinToFahrenheit = temperature => {
  return Math.round(((temperature * 9) / 5 - 459.67) * 10) / 10;
};
