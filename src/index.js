import React, { Component } from "react";
import ReactDOM from "react-dom";
import "./style/scss/WeatherApp.scss";
import WeatherContainer from "./components/WeatherContainer.jsx";

export default class App extends Component {
  render() {
    return (
      <div className="WeatherApp">
        <header className="WeatherApp-header"></header>
        <WeatherContainer />
        <div id="google"></div>
      </div>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("root"));
