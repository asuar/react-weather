import { getDateFromTimeZone, getDayFromDate } from "../utils/timezone-parser";
import {
  getTemperatureExtremes,
  getDailyWeather,
} from "../utils/forecast-parser";
import { getWeatherIcon } from "../utils/weather-icons";

export const parseForecastData = (rawData) => {
  //for each 5 days
  //each day has 8 times
  //get low and high temp of each day
  //get icon at noon of day
  let forecastData = {};
  let forecastDays = [];

  let index = 0;
  while (index < rawData.list.length) {
    if (
      rawData.list[index].dt_txt.slice(-8, -6) === "00" &&
      index + 8 <= rawData.list.length
    ) {
      let dayInfo = {
        dayABV: getDayFromDate(rawData.list[index].dt_txt, true),
        weather: getDailyWeather(
          rawData.list,
          index,
          rawData.list[index].dt_txt
        ),
        minTemp: getTemperatureExtremes(rawData.list, index, index + 8).min,
        maxTemp: getTemperatureExtremes(rawData.list, index, index + 8).max,
      };
      forecastDays.push(dayInfo);
    } else {
    }
    //page only styled for 4 days
    if (forecastDays.length >= 4) break;
    index++;
  }
  forecastData = { forecastDays };
  return forecastData;
};

export const parseWeatherData = (rawData) => {
  const currentTime = getDateFromTimeZone(rawData.timezone);

  return {
    city: rawData.name,
    country: rawData.sys.country,
    weather: rawData.weather[0].description,
    temperature: rawData.main.temp,
    humidity: rawData.main.humidity,
    windSpeed: rawData.wind.speed,
    time: currentTime,
    icon: getWeatherIcon(rawData.weather[0].id, currentTime),
  };
};
