import { parseWeatherData, parseForecastData } from "./handle-weather-data";
import { getLocationData } from "../utils/geolocator";

export const getCurrentWeather = async (cityName) => {
  let weatherUrl;
  let GPSLocationCity;
  let formattedCityName;

  try {
    if (typeof cityName === "undefined") {
      GPSLocationCity = await getLocationData();
      weatherUrl = `https://api.openweathermap.org/data/2.5/weather?lat=${GPSLocationCity.coords.latitude}&lon=${GPSLocationCity.coords.longitude}&APPID=f3c6137832e084f10271286436d3f1e0`;
    } else {
      formattedCityName = formatCityName(cityName);
      weatherUrl = `https://api.openweathermap.org/data/2.5/weather?q=${formattedCityName}&APPID=f3c6137832e084f10271286436d3f1e0`;
    }
  } catch (e) {
    //default
    formattedCityName = "Miami,FL";
    weatherUrl = `https://api.openweathermap.org/data/2.5/weather?q=${formattedCityName},us&APPID=f3c6137832e084f10271286436d3f1e0`;
    console.log("WeatherSearch: Unexpected error occurred");
  }

  const weatherResponse = await fetch(`${weatherUrl}`, {
    mode: "cors",
  }).then(async (response) => {
    if (response.ok) {
      return parseWeatherData(await response.json());
    } else {
      return "city not found";
    }
  });

  return weatherResponse;
};

export const getWeatherForecast = async (cityName) => {
  let url;
  let GPSLocationCity;
  let formattedCityName;

  try {
    if (typeof cityName === "undefined") {
      GPSLocationCity = await getLocationData();
      url = `https://api.openweathermap.org/data/2.5/forecast?lat=${GPSLocationCity.coords.latitude}&lon=${GPSLocationCity.coords.longitude}&APPID=${process.env.REACT_APP_OPEN_WEATHER_API_ID}`;
    } else {
      formattedCityName = formatCityName(cityName);
      url = `https://api.openweathermap.org/data/2.5/forecast?q=${formattedCityName}&APPID=${process.env.REACT_APP_OPEN_WEATHER_API_ID}`;
    }
  } catch (e) {
    formattedCityName = "Miami,FL,US";
    url = `https://api.openweathermap.org/data/2.5/forecast?q=${formattedCityName}&APPID=${process.env.REACT_APP_OPEN_WEATHER_API_ID}`;
    console.log("ForecastSearch: Unexpected error occurred");
  }

  const forecastResponse = await fetch(`${url}`, {
    mode: "cors",
  }).then(async (response) => {
    if (response.ok) {
      return parseForecastData(await response.json());
    } else {
      return "city not found";
    }
  });

  return forecastResponse;
};

export const getLocationImage = async (cityName, google) => {
  let GPSLocationCity;
  let formattedCityName;
  let ImageUrl;

  const service = new google.maps.places.PlacesService(
    document.getElementById("google")
  );

  try {
    if (typeof cityName === "undefined") {
      GPSLocationCity = await getLocationData();

      let cityCoor = new google.maps.LatLng(
        GPSLocationCity.coords.latitude,
        GPSLocationCity.coords.longitude
      );

      let request = {
        location: cityCoor,
        radius: "30000",
        type: ["locality"],
      };

      const getPhoto = (request) => {
        return new Promise((resolve, reject) => {
          service.nearbySearch(request, (results, status) => {
            if (status === google.maps.places.PlacesServiceStatus.OK) {
              resolve(results[0].photos[0].getUrl());
            } else {
              reject(status);
            }
          });
        });
      };

      ImageUrl = await getPhoto(request);
    } else {
      formattedCityName = formatCityName(cityName);

      let request = {
        query: formattedCityName,
        type: ["locality"],
      };

      const getPhoto = (request) => {
        return new Promise((resolve, reject) => {
          service.textSearch(request, (results, status) => {
            if (status === google.maps.places.PlacesServiceStatus.OK) {
              resolve(results[0].photos[0].getUrl());
            } else {
              reject(status);
            }
          });
        });
      };

      ImageUrl = await getPhoto(request);
    }
  } catch (e) {
    console.log("ImageSearch: Unexpected error occurred", ImageUrl);
  }
  return ImageUrl;
};

const formatCityName = (rawName) => {
  let formattedCityName;
  let regex = /(\w+)(?:,\s?)(\w{2})/gim;

  if (!isNaN(rawName)) {
    //check if zipcode
    formattedCityName = rawName + ",US";
  } else if (regex.test(rawName)) {
    //reset index from test
    regex.lastIndex = 0;
    //check if city, state
    let results = [...rawName.matchAll(regex)];
    formattedCityName =
      results[0][1] + "," + results[0][2].toUpperCase() + ",US";
  } else {
    //just a city
    formattedCityName = rawName + ",US";
  }
  return formattedCityName;
};
