# Weather

<div align="center">
<img src="weather.png" width="420">
</div>

This weather application was a tool for me to practice with React, OpenWeatherMap API, and GoogleMaps API.

## Features :star2:

:star: Modern UI design\
:star: Daily and 4 day forecast\
:star: Weather based on location\
:star: Visuals based on time of day and weather\
:star: Location background image from GoogleMaps API

Can be viewed **[here.](https://asuar.gitlab.io/react-weather/)**

## Technologies used 🛠️

- [React](https://es.reactjs.org/) - Front-End JavaScript library
- [React Bootstrap](https://react-bootstrap.github.io/) - Front-End framework rebuilt for React
- [OpenWeatherMap API](https://openweathermap.org/api) - A free API for weather data
- [GoogleMaps Javascript API](https://developers.google.com/maps/documentation/javascript/overview) - GoogleMap's Front-End API

## Try out my code 🚀

This code can be deployed directly on a service such as [Netlify](https://netlify.com). Here is how to install it locally:

### Prerequisites :clipboard:

[Git](https://git-scm.com)\
[NPM](http://npmjs.com)\
[React](https://es.reactjs.org/)

### Setup :wrench:

From the command line, first clone react-weather:

```bash
# Clone this repository
$ git clone https://gitlab.com/asuar/react-weather.git

# Go into the repository
$ cd react-weather

# Remove current origin repository
$ git remote remove origin
```

Then install the dependencies with NPM/Yarn:

Using NPM:

```bash
# Install dependencies
$ npm install

# Start development server
$ npm start
```

Once the server has started, the site can be accessed at `http://localhost:3000/`

## License 📄

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
